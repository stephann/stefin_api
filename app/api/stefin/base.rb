module Stefin
  class Base < Grape::API
    format :json

    mount Stefin::V1::Base
  end
end
