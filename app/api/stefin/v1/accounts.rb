module Stefin
  module V1
    class Accounts < Grape::API
      # GET /v1/accounts
      desc 'List accounts'

      get '/accounts' do
        accounts = Account.all
        present :accounts, accounts, with: AccountEntity
      end

      # GET v1/accounts/:id
      desc 'Show account details'

      params do
        requires :id, type: String
      end

      get '/accounts/:id' do
        account = Account.find(params[:id])
        present :account, account, with: AccountEntity
      end

      # POST v1/accounts
      desc 'Create a account'

      params do
        requires :account, type: Hash do
          requires :name, type: String
          requires :color, type: String
        end
      end

      post '/accounts' do
        account = Account.new(declared(params)[:account])

        if account.save
          present :account, account, with: AccountEntity
          status :created
        else
          present :errors, account.errors
          status :unprocessable_entity
        end
      end

      # PUT /v1/accounts/:id
      params do
        requires :id, type: String
        requires :account, type: Hash do
          optional :name, type: String
          optional :color, type: String
          at_least_one_of :name, :color
        end
      end

      put '/accounts/:id' do
        account = Account.find(params[:id])

        if account.update(declared(params, include_missing: false)[:account])
          present :account, account, with: AccountEntity
          status :ok
        else
          present :errors, account.errors
          status :unprocessable_entity
        end
      end

      # DELETE /v1/accounts/:id
      params do
        requires :id, type: String
      end

      delete '/accounts/:id' do
        account = Account.find(params[:id])

        account.destroy

        status :no_content
      end
    end
  end
end
