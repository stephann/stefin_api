module Stefin
  module V1
    class Records < Grape::API
      # GET /v1/records
      desc 'List records'

      paginate per_page: 10, max_per_page: 50

      get '/records' do
        records = paginate(Record.order(occurrence_date: :desc))
        present :records, records, with: RecordEntity
      end

      # GET v1/records/:id
      desc 'Show record details'

      params do
        requires :id, type: String
      end

      get '/records/:id' do
        record = Record.find(params[:id])
        present :record, record, with: RecordEntity, type: :detailed
      end

      # POST v1/records
      desc 'Create a record'

      params do
        requires :record, type: Hash do
          requires :account_id, type: String
          requires :occurrence_date, type: Date
          requires :group, type: String, values: %w[expense income]
          optional :description, type: String

          requires :record_items_attributes, type: Array do
            requires :name, type: String
            requires :amount, type: BigDecimal
            optional :description, type: String
            optional :tag_ids, type: Array
          end
        end
      end

      post '/records' do
        record_params = declared(params, include_missing: false)[:record]
        record = Record.new(record_params)

        if record.save
          present :record, record, with: RecordEntity
          status :created
        else
          present :errors, record.errors
          status :unprocessable_entity
        end
      end

      # PUT /v1/records/:id
      params do
        requires :id, type: String
        requires :record, type: Hash do
          optional :account_id, type: String
          optional :occurrence_date, type: Date
          optional :group, type: String, values: %w[expense income]
          optional :description, type: String

          requires :record_items_attributes, type: Array do
            optional :id, type: String
            optional :name, type: String
            optional :amount, type: BigDecimal
            optional :description, type: String
            optional :tag_ids, type: Array
            optional :_destroy, type: String
          end
        end
      end

      put '/records/:id' do
        record_params = declared(params, include_missing: false)[:record]
        record = Record.find(params[:id])

        if record.update(record_params)
          present :record, record, with: RecordEntity
          status :ok
        else
          present :errors, record.errors
          status :unprocessable_entity
        end
      end

      # DELETE /v1/records/:id
      params do
        requires :id, type: String
      end

      delete '/records/:id' do
        record = Record.find(params[:id])

        record.destroy

        status :no_content
      end
    end
  end
end
