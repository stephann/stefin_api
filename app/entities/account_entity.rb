class AccountEntity < Grape::Entity
  expose :id, documentation: {
    type: String
  }

  expose :name, documentation: {
    type: String
  }

  expose :balance, documentation: {
    type: String
  } do |account|
    account.balance.to_d.to_s
  end

  expose :formatted_balance, documentation: {
    type: String
  } do |account|
    account.balance.formatted
  end

  expose :color, documentation: {
    type: String
  }
end
