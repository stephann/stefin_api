class RecordEntity < Grape::Entity
  expose :id, documentation: {
    type: String
  }

  expose :occurrence_date, documentation: {
    type: Date
  }

  expose :group, documentation: {
    type: String
  }

  expose :amount, documentation: {
    type: String
  } do |record|
    record.amount.to_d.to_s
  end

  expose :formatted_amount, documentation: {
    type: String
  } do |record|
    record.amount.formatted
  end

  expose :description, documentation: {
    type: String
  }

  expose :account, using: AccountEntity, documentation: {
    type: Object
  }

  expose :record_items, as: :items, using: RecordItemEntity, if: { type: :detailed }, documentation: {
    type: Array
  }

  expose :tags, using: TagEntity, documentation: {
    type: Array
  }
end
