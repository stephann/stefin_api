class TagEntity < Grape::Entity
  expose :id, documentation: {
    type: String
  }

  expose :name, documentation: {
    type: String
  }

  expose :color, documentation: {
    type: String
  }
end
