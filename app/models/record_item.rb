class RecordItem < ApplicationRecord
  belongs_to :record, inverse_of: :record_items
  has_and_belongs_to_many :tags

  monetize :amount_cents

  validates_presence_of :name
  validates_presence_of :amount_cents
  validates_presence_of :amount_currency
  validates_numericality_of :amount, greater_than: 0
  validates_length_of :name, maximum: 32

  def self.total_amount
    sum(&:amount)
  end
end
