class Tag < ApplicationRecord
  has_and_belongs_to_many :record_items

  validates_presence_of :name
  validates_presence_of :color
  validates_uniqueness_of :name, case_sensitive: false
  validates_format_of :color, with: /\A#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})\z/
end
