class CreateAccounts < ActiveRecord::Migration[5.2]
  def change
    create_table :accounts, id: :uuid do |t|
      t.citext :name, null: false, unique: true
      t.monetize :balance, amount: { default: 0 }
      t.string :color, null: false

      t.timestamps
    end
  end
end
