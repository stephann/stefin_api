class CreateRecords < ActiveRecord::Migration[5.2]
  def change
    create_table :records, id: :uuid do |t|
      t.references :account, foreign_key: true, index: true, null: false, type: :uuid
      t.monetize :amount
      t.datetime :occurrence_date, null: false
      t.integer :group_cd, null: false
      t.string :description
    end
  end
end
