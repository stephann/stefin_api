class CreateJoinTableRecordItemTag < ActiveRecord::Migration[5.2]
  def change
    create_join_table :record_items, :tags, column_options: { type: :uuid } do |t|
      t.index %i[record_item_id tag_id]
      t.index %i[tag_id record_item_id]
    end
  end
end
