require 'rails_helper'

RSpec.describe Stefin::V1::Accounts do
  let(:invalid_params) { { name: nil, color: nil } }
  let(:valid_params) do
    FactoryBot.attributes_for(:account).slice(:name, :color)
  end

  context 'GET /v1/accounts' do
    let!(:account) { FactoryBot.create(:account) }

    it 'returns accounts list' do
      get '/v1/accounts'
      expect(response).to have_http_status(:ok)
      expect(response.body).to eq({ accounts: [AccountEntity.new(account)] }.to_json)
    end
  end

  context 'GET /v1/accounts/:id' do
    let(:account) { FactoryBot.create(:account) }

    it 'returns requested account' do
      get "/v1/accounts/#{account.id}"
      expect(response).to have_http_status(:ok)
      expect(response.body).to eq({ account: AccountEntity.new(account) }.to_json)
    end
  end

  context 'POST /v1/accounts' do
    context 'with valid attributes' do
      it 'creates a new account' do
        expect do
          post '/v1/accounts', params: { account: valid_params }
        end.to change(Account, :count).by(1)

        expect(response).to have_http_status(:created)

        expect(response.body).to eq({ account: AccountEntity.new(Account.last) }.to_json)
      end
    end

    context 'with invalid attributes' do
      it 'doesn`t create a new account' do
        expect do
          post '/v1/accounts', params: { account: invalid_params }
        end.to_not change(Account, :count)

        expect(response).to have_http_status(:unprocessable_entity)

        expect(JSON.parse(response.body)['errors']).to_not be_blank
      end
    end
  end

  context 'PUT /v1/accounts/:id' do
    let(:account) { FactoryBot.create(:account) }

    context 'with valid attributes' do
      it 'updates requested account' do
        put "/v1/accounts/#{account.id}", params: { account: valid_params }

        expect(response).to have_http_status(:ok)

        expect(response.body).to eq({ account: AccountEntity.new(account.reload) }.to_json)

        expect(account.name).to eq valid_params[:name]
      end
    end

    context 'with invalid attributes' do
      it 'doesn`t update requested account' do
        put "/v1/accounts/#{account.id}", params: { account: invalid_params }

        expect(response).to have_http_status(:unprocessable_entity)

        expect(JSON.parse(response.body)['errors']).to_not be_blank

        account.reload
        expect(account.name).to_not eq invalid_params[:name]
        expect(account.color).to_not eq invalid_params[:color]
      end
    end
  end

  context 'DELETE /v1/accounts/:id' do
    let!(:account) { FactoryBot.create(:account) }

    it 'destroys requested account' do
      expect do
        delete "/v1/accounts/#{account.id}"
      end.to change(Account, :count).by(-1)

      expect(response).to have_http_status(:no_content)

      expect(Account.find_by(id: account.id)).to be_nil
    end
  end
end
