require 'rails_helper'

RSpec.describe Stefin::V1::Records do
  let(:record_for_params) { FactoryBot.build(:record_with_items, with_tag: true) }
  let(:valid_params) do
    {
      account_id: record_for_params.account.id,
      occurrence_date: record_for_params.occurrence_date,
      group: record_for_params.group,
      description: record_for_params.description,
      record_items_attributes: record_for_params.record_items.map do |item|
        {
          name: item.name,
          amount: item.amount.to_d,
          describe: item.description,
          tag_ids: item.tags.map(&:id)
        }
      end
    }
  end
  let(:invalid_params) do
    {
      account_id: nil,
      occurrence_date: nil,
      group: nil,
      record_items_attributes: [{ name: nil, amount: 0 }]
    }
  end

  describe 'GET /v1/records' do
    let!(:record) { FactoryBot.create(:record_with_items) }

    it 'returns records list' do
      get '/v1/records'
      expect(response).to have_http_status(:ok)
      expect(response.body).to eq({ records: [RecordEntity.new(record)] }.to_json)
    end
  end

  describe 'GET /v1/records/:id' do
    let!(:record) { FactoryBot.create(:record_with_items) }

    it 'returns requested record' do
      get "/v1/records/#{record.id}"
      expect(response).to have_http_status(:ok)
      expect(response.body).to eq({ record: RecordEntity.new(record, type: :detailed) }.to_json)
    end
  end

  describe 'POST /v1/records' do
    context 'with valid attributes' do
      it 'creates a new record' do
        expect do
          post '/v1/records', params: { record: valid_params }
        end.to change(Record, :count).by(1)

        expect(response).to have_http_status(:created)

        expect(response.body).to eq({ record: RecordEntity.new(Record.last) }.to_json)
      end
    end

    describe 'with invalid attributes' do
      it 'doesn`t create a new record' do
        expect do
          post '/v1/records', params: { record: invalid_params }
        end.to_not change(Record, :count)

        expect(response).to have_http_status(:unprocessable_entity)

        expect(JSON.parse(response.body)['errors']).to_not be_blank
      end
    end
  end

  describe 'PUT /v1/records/:id' do
    let(:record) { FactoryBot.create(:record_with_items) }

    context 'with valid attributes' do
      it 'updates requested record' do
        put "/v1/records/#{record.id}", params: { record: valid_params }

        expect(response).to have_http_status(:ok)

        expect(response.body).to eq({ record: RecordEntity.new(record.reload) }.to_json)

        expect(record.occurrence_date).to eq valid_params[:occurrence_date]
        expect(record.description).to eq valid_params[:description]
        expect(record.account_id).to eq valid_params[:account_id]
      end
    end

    context 'with invalid attributes' do
      it 'doesn`t update requested record' do
        put "/v1/records/#{record.id}", params: { record: invalid_params }

        expect(response).to have_http_status(:unprocessable_entity)

        expect(JSON.parse(response.body)['errors']).to_not be_blank

        record.reload
        expect(record.occurrence_date).to_not eq valid_params[:occurrence_date]
        expect(record.description).to_not eq valid_params[:description]
        expect(record.account_id).to_not eq valid_params[:account_id]
      end
    end
  end

  context 'DELETE /v1/records/:id' do
    let!(:record) { FactoryBot.create(:record_with_items) }

    it 'destroys requested record' do
      expect do
        delete "/v1/records/#{record.id}"
      end.to change(Record, :count).by(-1)

      expect(response).to have_http_status(:no_content)

      expect(Record.find_by(id: record.id)).to be_nil
    end
  end
end
