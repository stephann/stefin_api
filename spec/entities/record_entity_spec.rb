require 'rails_helper'

RSpec.describe RecordEntity do
  let(:record) { FactoryBot.create(:record_with_items, with_tag: true) }
  let(:entity) { RecordEntity.new(record) }
  let(:detailed_entity) { RecordEntity.new(record, type: :detailed) }
  let(:serializable_hash) { entity.serializable_hash }
  let(:detailed_serializable_hash) { detailed_entity.serializable_hash }

  describe 'Fields' do
    it 'exposes id' do
      expect(serializable_hash[:id]).to eq record.id.to_s
      expect(detailed_serializable_hash[:id]).to eq record.id.to_s
    end

    it 'exposes occurrence_date' do
      expect(serializable_hash[:occurrence_date]).to eq record.occurrence_date
      expect(detailed_serializable_hash[:occurrence_date]).to eq record.occurrence_date
    end

    it 'exposes type' do
      expect(serializable_hash[:group]).to eq record.group
      expect(detailed_serializable_hash[:group]).to eq record.group
    end

    it 'exposes amount' do
      expect(serializable_hash[:amount]).to eq record.amount.to_d.to_s
      expect(detailed_serializable_hash[:amount]).to eq record.amount.to_d.to_s
    end

    it 'exposes formatted_amount' do
      expect(serializable_hash[:formatted_amount]).to eq record.amount.formatted
      expect(detailed_serializable_hash[:formatted_amount]).to eq record.amount.formatted
    end

    it 'exposes description' do
      expect(serializable_hash[:description]).to eq record.description
      expect(detailed_serializable_hash[:description]).to eq record.description
    end

    it 'exposes account' do
      expect(serializable_hash[:account]).to eq AccountEntity.new(record.account).serializable_hash
      expect(detailed_serializable_hash[:account]).to eq AccountEntity.new(record.account).serializable_hash
    end

    context 'when type is detailed' do
      it 'exposes items' do
        record_items_hash = record.record_items.map do |item|
          RecordItemEntity.new(item).serializable_hash
        end
        expect(serializable_hash[:items]).to_not eq record_items_hash
        expect(detailed_serializable_hash[:items]).to eq record_items_hash
      end
    end

    it 'exposes tags' do
      tags_hash = record.tags.map do |tag|
        TagEntity.new(tag).serializable_hash
      end

      expect(serializable_hash[:tags]).to eq tags_hash
    end
  end
end
