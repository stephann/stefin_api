require 'rails_helper'

RSpec.describe RecordItemEntity do
  let(:record) { FactoryBot.create(:record_with_items, items_count: 1, with_tag: true) }
  let(:record_item) { record.record_items.first }
  let(:entity) { RecordItemEntity.new(record_item) }
  let(:serializable_hash) { entity.serializable_hash }

  describe 'Fields' do
    it 'exposes id' do
      expect(serializable_hash[:id]).to eq record_item.id
    end

    it 'exposes name' do
      expect(serializable_hash[:name]).to eq record_item.name
    end

    it 'exposes amount' do
      expect(serializable_hash[:amount]).to eq record_item.amount.to_d.to_s
    end

    it 'exposes formatted_amount' do
      expect(serializable_hash[:formatted_amount]).to eq record_item.amount.formatted
    end

    it 'exposes description' do
      expect(serializable_hash[:description]).to eq record_item.description
    end

    it 'exposes tags' do
      tags_hash = record_item.tags.map do |tag|
        TagEntity.new(tag).serializable_hash
      end

      expect(serializable_hash[:tags]).to eq tags_hash
    end
  end
end
