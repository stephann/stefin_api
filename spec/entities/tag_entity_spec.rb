require 'rails_helper'

RSpec.describe TagEntity do
  let(:tag) { FactoryBot.create(:tag) }
  let(:entity) { TagEntity.new(tag) }
  let(:serializable_hash) { entity.serializable_hash }

  describe 'Fields' do
    it 'exposes id' do
      expect(serializable_hash[:id]).to eq tag.id
    end

    it 'exposes name' do
      expect(serializable_hash[:name]).to eq tag.name
    end

    it 'exposes color' do
      expect(serializable_hash[:color]).to eq tag.color
    end
  end
end
