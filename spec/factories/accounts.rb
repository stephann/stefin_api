FactoryBot.define do
  factory :account do
    name { Faker::Company.name }
    balance { Monetize.parse(Faker::Number.decimal(2)) }
    color { Faker::Color.hex_color }
  end
end
