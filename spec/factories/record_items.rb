FactoryBot.define do
  factory :record_item do
    record
    name { Faker::Lorem.word }
    description { Faker::Lorem.sentence }
    amount { Monetize.parse(Faker::Number.decimal(2)) }

    trait :with_tag do
      transient do
        tag { create(:tag) }
      end
      after(:build) do |item, evaluator|
        item.tags << evaluator.tag
      end
    end
  end
end
