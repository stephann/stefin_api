FactoryBot.define do
  factory :record do
    account
    amount { Monetize.parse(Faker::Number.decimal(2)) }
    occurrence_date { Faker::Date.backward(30) }
    description { Faker::Lorem.sentence }
    group { Record.groups.values.sample }

    trait :expense do
      group { :expense }
    end

    trait :income do
      group { :income }
    end

    factory :record_with_items do
      transient do
        items_count { 5 }
        with_tag { false }
        tag { nil }
      end

      after(:build) do |record, evaluator|
        if evaluator.with_tag
          tag = evaluator.tag || create(:tag)
          record.record_items = build_list(:record_item, evaluator.items_count, :with_tag, tag: tag, record: record)
        else
          record.record_items = build_list(:record_item, evaluator.items_count, record: record)
        end
      end
    end
  end
end
