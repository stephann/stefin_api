require 'rails_helper'

RSpec.describe Account, type: :model do
  describe 'Columns' do
    it { is_expected.to have_db_column(:name).of_type(:citext) }
    it { is_expected.to have_db_column(:color).of_type(:string) }
    it { is_expected.to monetize(:balance) }

    it do
      is_expected.to have_db_column(:balance_cents).of_type(:integer).with_options(default: 0, null: false)
    end

    it do
      is_expected.to have_db_column(:balance_currency).of_type(:string).with_options(default: 'BRL', null: false)
    end
  end

  describe 'Relations' do
    it { is_expected.to have_many(:records).dependent(:destroy) }
  end

  describe 'Validations' do
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_presence_of(:balance_cents) }
    it { is_expected.to validate_presence_of(:balance_currency) }
    it { is_expected.to validate_presence_of(:color) }

    it do
      FactoryBot.create(:account)
      is_expected.to validate_uniqueness_of(:name).case_insensitive
    end

    it { is_expected.to allow_values('#FF0099', '#AACC11', '#FFF', '#000').for(:color) }
    it { is_expected.to_not allow_values('FF0099', '#GG0000', '#FFFF', '#GGG').for(:color) }
  end

  describe 'Methods' do
    describe '#update_balance' do
      let(:account) { FactoryBot.create(:account) }
      let(:random_balance) { Monetize.parse(Faker::Number.decimal(2)) }

      it 'updates balance with calculated balance' do
        allow(account).to receive(:calculated_balance).and_return(random_balance)
        account.update_balance
        account.reload
        expect(account.balance).to eq random_balance
      end
    end

    describe '#calculated_balance' do
      let!(:account) { FactoryBot.create(:account) }

      let!(:income_records) do
        FactoryBot.create_list(:record_with_items, 3, :income, account: account)
      end

      let!(:expense_records) do
        FactoryBot.create_list(:record_with_items, 3, :expense, account: account)
      end

      it 'returns sum of all records' do
        expected_amount = income_records.map(&:amount).sum - expense_records.map(&:amount).sum
        expect(account.calculated_balance).to eq expected_amount
      end
    end
  end
end
