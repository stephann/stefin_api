require 'rails_helper'

RSpec.describe RecordItem, type: :model do
  describe 'Columns' do
    it { is_expected.to have_db_column(:name).of_type(:string) }
    it { is_expected.to have_db_column(:description).of_type(:string) }
    it { is_expected.to have_db_column(:amount_cents).of_type(:integer) }
    it { is_expected.to have_db_column(:amount_currency).of_type(:string) }
    it { is_expected.to monetize(:amount) }
  end

  describe 'Relations' do
    it { is_expected.to belong_to(:record) }
    it { is_expected.to have_and_belong_to_many(:tags) }
  end

  describe 'Validations' do
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_presence_of(:amount_cents) }
    it { is_expected.to validate_presence_of(:amount_currency) }
    it { is_expected.to validate_numericality_of(:amount).is_greater_than(0) }
    it { is_expected.to validate_length_of(:name).is_at_most(32) }
  end

  describe 'Class methods' do
    describe '.total_amount' do
      let!(:record) { FactoryBot.create(:record_with_items, items_count: 2) }
      let!(:record_item_1) { record.record_items.first }
      let!(:record_item_2) { record.record_items.last }

      it 'returns a sum of all record`s items amount' do
        expected_amount = record_item_1.amount + record_item_2.amount
        expect(record.record_items.total_amount).to eq expected_amount
      end
    end
  end
end
