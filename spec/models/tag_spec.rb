require 'rails_helper'

RSpec.describe Tag, type: :model do
  describe 'Columns' do
    it { is_expected.to have_db_column(:name).of_type(:citext) }
    it { is_expected.to have_db_column(:color).of_type(:string) }
  end

  describe 'Relations' do
    it { is_expected.to have_and_belong_to_many(:record_items) }
  end

  describe 'Validations' do
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_presence_of(:color) }

    it do
      FactoryBot.create(:tag)
      is_expected.to validate_uniqueness_of(:name).case_insensitive
    end

    it { is_expected.to allow_values('#FF0099', '#AACC11', '#FFF', '#000').for(:color) }
    it { is_expected.to_not allow_values('FF0099', '#GG0000', '#FFFF', '#GGG').for(:color) }
  end
end
